package edu.epam.fop.jpa.social.service;

import edu.epam.fop.jpa.social.entity.Comment;
import edu.epam.fop.jpa.social.entity.Post;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.function.Consumer;

/**
 * Implementation of the {@link SocialMediaService} interface.
 */
public class SocialMediaServiceImpl implements SocialMediaService {
    /**
     * The entity manager factory.
     */
    private final EntityManager entityManager;

    /**
     * Constructs a new instance of the {@link SocialMediaServiceImpl} class.
     */
    public SocialMediaServiceImpl(EntityManager entityManagerFactory) {
        this.entityManager = entityManagerFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer createPost(Consumer<Post> postConfig) {
        try {
            entityManager.getTransaction().begin();

            Post post = new Post();
            postConfig.accept(post);
            entityManager.persist(post);

            entityManager.getTransaction().commit();
            return post.getPostId().intValue();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer addComment(Integer postId, Consumer<Comment> commentConfig) {
        try {
            entityManager.getTransaction().begin();

            Post post = entityManager.find(Post.class, postId);
            if (post == null) {
                throw new IllegalArgumentException("Post with ID "
                    + postId + " not found");
            }

            Comment comment = new Comment();
            commentConfig.accept(comment);
            comment.setPost(post);
            // todo set bi-direction
            post.getComments().add(comment);

            entityManager.persist(comment);

            entityManager.getTransaction().commit();
            return comment.getCommentId().intValue();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removePost(Integer postId) {
        try {
            entityManager.getTransaction().begin();

            Post post = entityManager.find(Post.class, postId);
            if (post != null) {
                entityManager.remove(post);
            } else {
                throw new IllegalArgumentException("Post with ID "
                    + postId + " not found");
            }

            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeComment(Integer commentId) {
        try {
            entityManager.getTransaction().begin();

            Comment comment = entityManager.find(Comment.class, commentId);
            if (comment != null) {
                //TOdo unbind bind relation from both objects - from both sides
                comment.getPost().getComments().remove(comment);
                entityManager.remove(comment);
            } else {
                throw new IllegalArgumentException("Comment with ID "
                    + commentId + " not found");
            }

            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
    }
}

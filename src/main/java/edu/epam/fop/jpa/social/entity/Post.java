package edu.epam.fop.jpa.social.entity;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a post in a social media application.
 */
@Entity
@Table(name = "posts")
public class Post {
    /**
     * The unique identifier of the post.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private Long postId;

    /**
     * The content of the post.
     */
    @Column(name = "content")
    private String content;

    /**
     * The author of the post.
     */
    @Column(name = "author")
    private String author;

    /**
     * The comments on the post.
     */
    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> comments = new ArrayList<>();

    /**
     * Constructs a Post object with specified parameters.
     *
     * @param postId   The unique identifier for the post.
     * @param content  The content of the post.
     * @param author   The author of the post.
     * @param comments The comments on the post.
     */
    public Post(Long postId, String content, String author, List<Comment> comments) {
        this.postId = postId;
        this.content = content;
        this.author = author;
        this.comments = comments;
    }

    /**
     * Constructs a Post object with default parameters.
     */
    public Post() {
    }

    /**
     * Returns the unique identifier of the post.
     *
     * @return The unique identifier of the post.
     */
    public Long getPostId() {
        return postId;
    }

    /**
     * Sets the unique identifier of the post.
     *
     * @param postId The unique identifier of the post.
     */
    public void setPostId(Long postId) {
        this.postId = postId;
    }

    /**
     * Returns the content of the post.
     *
     * @return The content of the post.
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the content of the post.
     *
     * @param content The content of the post.
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Returns the author of the post.
     *
     * @return The author of the post.
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets the author of the post.
     *
     * @param author The author of the post.
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Returns the comments on the post.
     *
     * @return The comments on the post.
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * Sets the comments on the post.
     *
     * @param comments The comments on the post.
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}

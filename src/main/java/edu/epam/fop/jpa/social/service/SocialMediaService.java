package edu.epam.fop.jpa.social.service;

import edu.epam.fop.jpa.social.entity.Comment;
import edu.epam.fop.jpa.social.entity.Post;

import java.util.function.Consumer;

public interface SocialMediaService {
    /**
     * Creates a new post with the specified content and author.
     *
     * @param postConfig The configuration of the post.
     * @return The unique identifier of the created post.
     */
    Integer createPost(Consumer<Post> postConfig);

    /**
     * Adds a new comment to the post with the specified identifier.
     *
     * @param postId        The unique identifier of the post.
     * @param commentConfig The configuration of the comment.
     * @return The unique identifier of the created comment.
     */
    Integer addComment(Integer postId, Consumer<Comment> commentConfig);

    /**
     * Removes the post with the specified identifier.
     *
     * @param postId The unique identifier of the post to remove.
     */
    void removePost(Integer postId);

    /**
     * Removes the comment with the specified identifier.
     *
     * @param commentId The unique identifier of the comment to remove.
     */
    void removeComment(Integer commentId);
}

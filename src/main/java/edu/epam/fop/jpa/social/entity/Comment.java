package edu.epam.fop.jpa.social.entity;

import jakarta.persistence.*;

/**
 * Represents a comment on a post.
 */
@Entity
@Table(name = "comments")
public class Comment {

    /**
     * The unique identifier of the comment.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private Long commentId;

    /**
     * The post to which the comment is attached.
     */
    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    /**
     * The text of the comment.
     */
    @Column(name = "comment_text")
    private String commentText;

    /**
     * The user who made the comment.
     */
    @Column(name = "commenter")
    private String commenter;

    /**
     * Constructs a Comment object with specified parameters.
     *
     * @param commentId    The unique identifier for the comment.
     * @param post         The post to which the comment belongs.
     * @param commentText  The text content of the comment.
     * @param commenter    The author of the comment.
     */
    public Comment(Long commentId, Post post, String commentText, String commenter) {
        this.commentId = commentId;
        this.post = post;
        this.commentText = commentText;
        this.commenter = commenter;
    }

    /**
     * Constructs a Comment object with default parameters.
     */
    public Comment() {
    }

    /**
     * Returns the unique identifier of the comment.
     *
     * @return The unique identifier of the comment.
     */
    public Long getCommentId() {
        return commentId;
    }

    /**
     * Sets the unique identifier of the comment.
     *
     * @param commentId The unique identifier of the comment.
     */
    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    /**
     * Returns the post to which the comment is attached.
     *
     * @return The post to which the comment is attached.
     */
    public Post getPost() {
        return post;
    }

    /**
     * Sets the post to which the comment is attached.
     *
     * @param post The post to which the comment is attached.
     */
    public void setPost(Post post) {
        this.post = post;
    }

    /**
     * Returns the text of the comment.
     *
     * @return The text of the comment.
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * Sets the text of the comment.
     *
     * @param commentText The text of the comment.
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * Returns the user who made the comment.
     *
     * @return The user who made the comment.
     */
    public String getCommenter() {
        return commenter;
    }

    /**
     * Sets the user who made the comment.
     *
     * @param commenter The user who made the comment.
     */
    public void setCommenter(String commenter) {
        this.commenter = commenter;
    }
}

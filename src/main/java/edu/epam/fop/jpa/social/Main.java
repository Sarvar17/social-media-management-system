package edu.epam.fop.jpa.social;

import edu.epam.fop.jpa.social.service.SocialMediaService;
import edu.epam.fop.jpa.social.service.SocialMediaServiceImpl;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

/**
 * Main class to demonstrate the usage of the SocialMediaService.
 */
public class Main {
    /**
     * Main method to demonstrate the usage of the SocialMediaService.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence
                .createEntityManagerFactory("SocialMediaPU");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        // Instantiate SocialMediaService
        SocialMediaService socialMediaService = new SocialMediaServiceImpl(entityManager);

        // Creating a post
        Integer postId = socialMediaService.createPost(post -> {
            post.setContent("bla bla bla");
            post.setAuthor("admin");
        });
        Integer postId2 = socialMediaService.createPost(post -> {
            post.setContent("bla bla bla2");
            post.setAuthor("admin2");
        });

        // Creating multiple comments for this post
        Integer commentId1 = socialMediaService.addComment(postId, comment -> {
            comment.setCommentText("bla bla bla");
            comment.setCommenter("coolUser");
        });

        Integer commentId2 = socialMediaService.addComment(postId, comment -> {
            comment.setCommentText("bla bla bla 2");
            comment.setCommenter("coolUser2");
        });
        Integer commentId21 = socialMediaService.addComment(postId2, comment -> {
            comment.setCommentText("bla bla bla21");
            comment.setCommenter("coolUser21");
        });

        Integer commentId22 = socialMediaService.addComment(postId2, comment -> {
            comment.setCommentText("bla bla bla 22");
            comment.setCommenter("coolUser22");
        });

        // Deleting the comments
        socialMediaService.removeComment(commentId2);

        // Deleting the post along with its comments
        socialMediaService.removePost(postId2);

        entityManager.close();
        entityManagerFactory.close();
    }
}

package edu.epam.fop.jpa.social.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.junit.jupiter.api.*;

/**
 * Functional test for the orphan removal feature.
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OrphanRemovalFuncTest {
  /**
   * The service under test.
   */
  private static SocialMediaService socialMediaService;
  /**
   * The ID of the post created in the test.
   */
  private static Integer postId;
  /**
   * The first ID of the comment created in the test.
   */
  private static Integer commentId1;
  /**
   * The second ID of the comment created in the test.
   */
  private static Integer commentId2;

  private static EntityManager entityManager;
  private static EntityManagerFactory entityManagerFactory;

  /**
   * Set up the test environment.
   */
  @BeforeAll
  static void setUp() {
    entityManagerFactory = Persistence
            .createEntityManagerFactory("SocialMediaPU");
    entityManager = entityManagerFactory.createEntityManager();
    socialMediaService = new SocialMediaServiceImpl(entityManager);
  }

  /**
   * Create a post with comments.
   * Check that the post and its comments are added to the database.
   */
  @Test
  @Order(1)
  void createPostWithComments() {
    // Create a post
    postId = socialMediaService.createPost(post -> {
      post.setContent("Test post content");
      post.setAuthor("Test author");
    });

    // Add comments to the post
    commentId1 = socialMediaService.addComment(postId, comment -> {
      comment.setCommentText("Test comment 1");
      comment.setCommenter("User 1");
    });

    commentId2 = socialMediaService.addComment(postId, comment -> {
      comment.setCommentText("Test comment 2");
      comment.setCommenter("User 2");
    });

    // Verify that the post and its comments are added to the database
    assertNotNull(postId);
    assertNotNull(commentId1);
    assertNotNull(commentId2);
  }

  /**
   * Remove the post and check that the post and its comments are deleted from the database.
   */
  @Test
  @Order(2)
  void removePost_ShouldDeletePostAndAssociatedComments() {
    // Remove the post
    socialMediaService.removePost(postId);

    // Verify that the post and its comments are deleted from the database
    assertThrows(IllegalArgumentException.class, () -> {
      socialMediaService.removePost(postId);
    });
    assertThrows(IllegalArgumentException.class, () -> {
      socialMediaService.removeComment(commentId1);
    });
    assertThrows(IllegalArgumentException.class, () -> {
      socialMediaService.removeComment(commentId2);
    });
  }

  @AfterAll
  public static void tearDown(){
    entityManager.close();
    entityManagerFactory.close();
  }
}

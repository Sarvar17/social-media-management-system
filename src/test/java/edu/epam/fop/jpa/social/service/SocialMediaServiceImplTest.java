package edu.epam.fop.jpa.social.service;

import static org.junit.jupiter.api.Assertions.*;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.junit.jupiter.api.*;

/**
 * Test class for the SocialMediaService interface implementation.
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SocialMediaServiceImplTest {
  /**
   * The service to be tested.
   */
  private static SocialMediaService socialMediaService;
  /**
   * The ID of the post created in the test.
   */
  private static Integer postId;
  /**
   * The ID of the comment created in the test.
   */
  private static Integer commentId;
  private static EntityManager entityManager;
  private static EntityManagerFactory entityManagerFactory;

  /**
   * Set up the test environment.
   */
  @BeforeAll
  static void setUp() {
    entityManagerFactory = Persistence
            .createEntityManagerFactory("SocialMediaPU");
    entityManager = entityManagerFactory.createEntityManager();
    socialMediaService = new SocialMediaServiceImpl(entityManager);
  }

  /**
   * Test the creation of a post.
   */
  @Test
  @Order(1)
  void createPost() {
    // Create a post
    postId = socialMediaService.createPost(post -> {
      post.setContent("Test post content");
      post.setAuthor("Test author");
    });

    assertNotNull(postId);
  }

  /**
   * Test the addition of a comment to a post.
   */
  @Test
  @Order(2)
  void addComment() {
    // Add a comment to the post
    commentId = socialMediaService.addComment(postId, comment -> {
      comment.setCommentText("Test comment");
      comment.setCommenter("User 1");
    });

    assertNotNull(commentId);
  }

  /**
   * Test the removal of a comment.
   */
  @Test
  @Order(3)
  void removeComment() {
    // Remove the comment
    socialMediaService.removeComment(commentId);

    // Verify that the comment have been removed
    assertThrows(IllegalArgumentException.class, () -> {
      socialMediaService.removeComment(commentId);
    });
  }

  /**
   * Test the removal of a post.
   */
  @Test
  @Order(4)
  void removePost() {
    // Remove the post
    socialMediaService.removePost(postId);

    // Verify that the post have been removed
    assertThrows(IllegalArgumentException.class, () -> {
      socialMediaService.removePost(postId);
    });
  }

  @AfterAll
  public static void tearDown(){
    entityManager.close();
    entityManagerFactory.close();
  }
}